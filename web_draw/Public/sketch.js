var socket;

// create an instance of scribble and set a few parameters
var scribble = new Scribble();
scribble.bowing = 0.1;
scribble.roughness = 1.5;

var DrawMode = true;
var SwitchViewButton;
var SaveButton;
var resetButton;
var nameBox;
var numBox;
var colBox;

var RecordButton
var c;
var username;
var num = 1;
var fillC;

var recordDuration = 10000;

var publicIP = '192.168.0.101';

function preload() {
  img = loadImage('bin/map.png');
  ready = loadImage('bin/ready.png')
  recording = loadImage('bin/recording.png')
}

function setup() {
   	c = createCanvas(displayWidth, displayHeight - 120);
//  createCanvas(displayWidth, displayHeight - 120);
  	background(256)
    // socket = io.connect('http://127.0.0.1:3030');
  	socket = io.connect('http://' + publicIP + ':3030');
//  setMoveThreshold(threshold);
    showSilhouette();
  	//image(img,displayWidth/4 , 0,displayWidth/2, displayHeight - 120);
  	noStroke();
  	fillC = color(252,86,54,80);

    SwitchViewButton = createButton('record gesture')
    SwitchViewButton.style('font-size', '16px');
    SwitchViewButton.style('max-width', '80px');
    SwitchViewButton.position(width - 100, 10);
    SwitchViewButton.mousePressed(switchView);

	  SaveButton = createButton('Save');
    SaveButton.style('font-size', '16px');
  	SaveButton.position(10, 70);
  	SaveButton.mousePressed(imgToServer);

  	resetButton = createButton('clear');
    resetButton.style('font-size', '16px');
    resetButton.position(10, height-70);
    resetButton.mousePressed(resetCanvas);

	nameBox = createSelect();
  nameBox.style('font-size', '16px');
	nameBox.position(10,10);
	nameBox.option('name');
  	nameBox.option('Deva');
  	nameBox.option('Einav');
  	nameBox.option('Eleonora');
  	nameBox.option('Ella');
  	nameBox.option('Hanna');
  	nameBox.option('Kadri');
  	nameBox.option('Juan');
  	nameBox.option('Maria');
  	nameBox.option('Outi');
  	nameBox.option('Simona');
  	nameBox.option('Sylvia');
  	nameBox.option('Zjana');
  	nameBox.changed(name_inp_event);

	numBox = createSelect();
  numBox.style('font-size', '16px');
	numBox.position(10,40);
	for (i = 0 ; i < 5; i++){
		numBox.option((i+1).toString());
	}
	numBox.option('random');
  	numBox.changed(num_inp_event);

	colBox = createSelect();
  colBox.style('font-size', '16px');
	colBox.position(10,height-40);
	colBox.option('red');
	colBox.option('blue');
	colBox.changed(col_inp_event);
}

function draw() {
}

function touchStarted() {
	drawToCanvas();
}

function touchMoved(){
    drawToCanvas();
}

function drawToCanvas(){
    if (         // right of the left edge AND
      mouseX <= 90 &&    // left of the right edge AND
      mouseY <= 90) {    // above the bottom
      return false;
    }else{
      if(DrawMode){
    	   fill(fillC);
    	   scribble.scribbleEllipse(mouseX,mouseY,8,8);
      }
	}
}

function name_inp_event() {
	username = nameBox.value()
	console.log(nameBox.value());
}

function num_inp_event() {
	console.log(numBox.value());
	if(numBox.value() == "random"){
		num = Math.floor(Math.random() * (1000 - 0 + 1)) + 0;
	}else{
		num = parseInt(numBox.value());
	}
}

function col_inp_event() {
	if(colBox.value() == "red"){
		fillC = color(252,86,54,80)
	}if(colBox.value() == "blue"){
		fillC = color(57,216,249,80)
	}
}

function switchView(){
  DrawMode = !DrawMode;
  newImg = DrawMode ? img : ready;
  image(newImg, 0, 0,displayWidth, displayHeight - 120);
  if (!DrawMode)
    startRecrod();
}

async function startRecrod(){
  await wait(3000);
  socket.emit('record_start',num);
  image(recording, 0, 0,displayWidth, displayHeight - 120);
  await wait(5000);
  socket.emit('record_stop',num);
  switchView();
}


function imgToServer(){
  if (DrawMode){
  	var canvas64 = canvasToBase64();
      socket.emit('canvas',canvas64,username,num);
  }
}

function resetCanvas(){
  showSilhouette();
}

function showSilhouette(){
  image(img, displayWidth/4, 0,displayWidth/2, displayHeight - 120);
}

function canvasToBase64 () {
    //console.log( canvas.toDataURL() );
  canvas_ouput = cropCanvas(canvas,displayWidth/2,0,displayWidth,(displayHeight-120)*2)
	var canvasStr = canvas_ouput.toDataURL();
	return canvasStr;
}

function wait(time) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, time);
    });
}

function cropCanvas (sourceCanvas,left,top,width,height) {
    let destCanvas = document.createElement('canvas');
    destCanvas.width = width;
    destCanvas.height = height;
    destCanvas
      .getContext("2d")
      .drawImage(sourceCanvas,
        left,top,width,height,  // source rect with content to crop
        0,0,width,height);      // newCanvas, same size as source rect
    return destCanvas;
}
