var express = require('express');
var app = express ();
var server = app.listen(3030, "0.0.0.0");
var path = require('path');
var player = require('play-sound')(opts = {})
var shell = require('shelljs');
var publicIP = '192.168.0.101';

app.use(express.static('Public'));

console.log("running mydraw");

var osc = require('node-osc');
var client = new osc.Client('127.0.0.1', 1212);
// var client = new osc.Client(publicIP, 1212);

var socket = require('socket.io');
var io = socket(server);

io.sockets.on('connection', newConnection);

function newConnection(socket) {
  console.log('new connection ' + socket.id);
  socket.on('canvas', saveCanvas);
  socket.on('record_start', startRecording);
  socket.on('record_stop', stopRecording);

 function saveCanvas(canvas64, name, seq_id) {
	var imgData = canvas64;
	var base64Data = imgData.replace(/^data:image\/png;base64,/, "");
	var child_path = name+"/"+seq_id+".png";
	require("fs").writeFile("../img_save/data/"+child_path, base64Data, 'base64',
	function(err, data) {
	if (err) {
    	console.log('err', err);
		client.send('/error', "error, check console");
		player.play('../img_save/data/error.wav', function(err){
  			if (err) throw err
		})
	}else{
		console.log('saved image as: ' + child_path);
    player.play('../img_save/data/notification.wav', function(err){
        if (err) throw err
    })
	 	shell.exec('../../latent_steps_visualizer/sketch_feature_extractor/process_images.sh')
		client.send('/new_drawing', child_path);
	}
});
}

function startRecording(gesture_id){
		client.send('/start_record', gesture_id);
    player.play('Public/bin/beep.wav', function(err){
        if (err) throw err
    })
	}

function stopRecording(gesture_id){
		client.send('/stop_record', gesture_id);
    player.play('Public/bin/beep2.wav', function(err){
        if (err) throw err
    })
	}
}
