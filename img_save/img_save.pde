import oscP5.*;
import netP5.*;
import ddf.minim.*;

OscP5 oscP5;
String new_img_path;
boolean not_null = false;

Minim minim;
AudioPlayer notif_sound;
AudioPlayer error_sound;

void setup() {
  size(128, 256);
  oscP5 = new OscP5(this, 1212);
  
  minim = new Minim(this);
  notif_sound = minim.loadFile("notification.wav");
  error_sound = minim.loadFile("error.wav");
}

void draw() {
  if (not_null) {
    PImage new_img = loadImage(new_img_path);
    image(new_img, 0, 0, width, height);
  }
}

void oscEvent(OscMessage theOscMessage) {  
  if (theOscMessage.checkAddrPattern("/new_drawing")==true) {
    print(theOscMessage.get(0).stringValue());
    new_img_path = theOscMessage.get(0).stringValue();
    notif_sound.rewind();
    notif_sound.play();
    not_null = true;
  }
  if (theOscMessage.checkAddrPattern("/error")==true) {
        print(theOscMessage.get(0).stringValue());
        error_sound.rewind();
        error_sound.play();
  }
}
